static const char *id="@(#) $Id: ShmManager.cc,v 1.1.12.1 2013/12/11 12:02:30 shijuc Exp $";
//--------------------------------------------------------------
// NAME      SharedMemory.cc
// fullpath/SharedMemory.cc --
//          Inter process communication system using sockets
//
// COPYRIGHT
// BITL -- Copyright (C) 2001 BPL Innovision Technologies Ltd.,
// All rights reserved. No part of this computer program
// may be used or reproduced in any form by any
// means without prior written permission of
// BPL Innovision Technologies Ltd.
//
// Originated : 29-NOV-2001  Sadiq Mohammed
//-------------------------------------------------------------
#include <iostream.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/times.h>
#include <assert.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "ShmManager.h"

//-------------------------------------------------------------
typedef size_t SIZE_T;

//-------------------------------------------------------------
CTrace gShmEmTrace("TRACE_SHMEM", false);

//-------------------------------------------------------------
#define MAX_APPS        50
// free rawNode objects are linked on a circular list with the
// next and prev pointers.
// Note that the size fields are always a multiple of SHMEM_ALIGNMENT,
// which allows for the low order bits to be used for something else.
#define IN_USE_BIT    1
#define DUMMY_BITS    2 | 4
#define SPECIAL_BITS  (IN_USE_BIT|DUMMY_BITS)
#define SIZE_BITS    ~(SPECIAL_BITS)

//-------------------------------------------------------------
struct rawNode
{
   private:
      SIZE_T mPrevSize;
      SIZE_T mSize;
   public:
      rawNode* next;
      rawNode* prev;
   public:
      SIZE_T Size()
         // mask off the special bits in the size field
      {
         return mSize & SIZE_BITS;
      }

      SIZE_T PrevSize()
         // The prevSize field does not have any settings for the
         // lower order bits.
      {
         return mPrevSize;
      }

      int InUse ()
         // returns if the current node is allocated(i.e. InUse)
      {
         return mSize & IN_USE_BIT;
      }

      void ClearInUse ()
         // mark the current node as no longer in use
      {
         mSize = mSize & SIZE_BITS;
      }

      void SetInUse ()
         // mark the current node as allocated
      {
         mSize = mSize | IN_USE_BIT;
      }

      void SetSize (SIZE_T nsize)
         // set the size of the current node. This does not
         // disturb the InUse bit.
      {
         if ( nsize )
            assert (nsize % (SPECIAL_BITS + 1) == 0 );
         mSize = (mSize & SPECIAL_BITS) | nsize;
      }

      void SetPrevSize (SIZE_T nsize)
         // Set the previous size of the current node.
      {
         if ( nsize )
            assert (nsize % (SPECIAL_BITS + 1) == 0 );
         mPrevSize = nsize;
      }

      void UnLink()
         // remove the current node from whatever double-linked-list
         // it is currently a member.
      {
         next->prev = prev;
         prev->next = next;
      }

      void Link(rawNode* freelist)
         // put the current node onto the passed circular, doubly
         // linked list. The passed pointer <freelist> points to
         // the 'sentinel' element of the freelist. The current
         // node is inserted at the head, i.e. right after the
         // 'sentinel' node.
      {
         rawNode* n = freelist->next;
         rawNode* p = n->prev;
         next = n;
         prev = p;
         n->prev = this;
         freelist->next = this;
      }

      rawNode* PreviousNode()
         // return the previous node if there is one.
      {
         if (mPrevSize)
            return (rawNode*)((TEXT*)this - (mPrevSize & SIZE_BITS));
         else
            return 0;
      }

      rawNode* NextNode()
         // return the next node.
      {
         return (rawNode*)((TEXT*)this + (mSize & SIZE_BITS));
      }
};

const unsigned int MIN_TRUNCATE_SIZE = sizeof(rawNode);
// minimun size needs to be able to hold at least
// a rawNode instance

// The raw nodes when allocated should be aligned. The choosen
// alignment is an 8-byte alignment in anticipation of the
// 64-bit architecture.
#define SHMEM_ALIGNMENT 8
#define SHMEM_ALIGN_MASK (SHMEM_ALIGNMENT - 1)

//-------------------------------------------------------------
// METHOD
//    memAlign
//
// DESCRIPTION
// return an address that is properly aligned and >= than the
// passed in address.
//
// PARAMETER
//      p                        - TEXT
//
// RETURN
//       *address              - TEXT
//
// THROWS
//-------------------------------------------------------------
inline TEXT* MemAlign(TEXT* p)
{
   return (TEXT*)((unsigned int )(p + SHMEM_ALIGN_MASK ) & ~SHMEM_ALIGN_MASK);
}

//-------------------------------------------------------------
// METHOD
//    sizeRoundUP
//
// DESCRIPTION
// Requests for bytes are to be converted so that these are a
// multiple of SHMEM_ALIGNMENT and have at least enough capacity
// to hold a <node>
//
// PARAMETER
//      req                        - SIZE_T
//
// RETURN
//       need                    - SIZE_T
//
// THROWS
//-------------------------------------------------------------
inline SIZE_T SizeRoundUP(SIZE_T req)
{
   SIZE_T need = (req + SHMEM_ALIGN_MASK) & ~SHMEM_ALIGN_MASK;
   if ( need < MIN_TRUNCATE_SIZE)
      need = MIN_TRUNCATE_SIZE;
   return need;
}  // sizeRoundUP

//-------------------------------------------------------------
// METHOD
//    sizelign
//
// DESCRIPTION
// align the size to the SHMEM_ALIGN_MASK.
//
// PARAMETER
//      req                        - SIZE_T
//
// RETURN
//       need                    - SIZE_T
//
// THROWS
//-------------------------------------------------------------
size_t ShmManager::SizeAlign(size_t req)
{
   return (req + SHMEM_ALIGN_MASK) & ~SHMEM_ALIGN_MASK;
}

inline rawNode* rawnode_at_offset(const rawNode* rnode, int offset)
{
   return (rawNode*)((TEXT*)rnode + offset);
}

#define rawnode2mem(n) ((TEXT*)(n)+2*sizeof(SIZE_T))
#define mem2rawnode(m) ((rawNode*)((TEXT*)(m)-2*sizeof(SIZE_T)))

class ShMemHdr
{
   public:

      int mNumFree;            // # nodes on free list
      int mFreeSpace;          // # bytes
      int mFreePercent;        // for warning/error message reporting
      time_t mLastMsgReport;      // for error reporting, reduces the
      // number of messages to at most 1/sec

      unsigned int totalShMemSize;       // Configuration file driven.
      unsigned int totalUsableSize;      // always less than totalShMemSize

      // The following 3 fields are read from
      // a configuration file.
      unsigned int maxPoolSize;
      unsigned int lastPoolSize;
      unsigned int mNumPools;

      rawNode* mFreeList;
      // The freelist points to a rawNode, which is the
      // sentinel element of the circular list. This first
      // element will never be deleted, because it has a size
      // that does not allow for any user data.
      // It is also marked to always be IN_USE

      // All returned data must be contained
      // between begin and end of each pool.
      TEXT* mSegmentStart[ MAX_NMB_SHMEM_POOLS ];
      TEXT* mSegmentEnd[ MAX_NMB_SHMEM_POOLS ];

      unsigned int poolKey[ MAX_NMB_SHMEM_POOLS];
      int poolId[ MAX_NMB_SHMEM_POOLS ];
      void* poolPtr[ MAX_NMB_SHMEM_POOLS ];
      SIZE_T poolSize[ MAX_NMB_SHMEM_POOLS ];

      int numApps;                       // # of registerd applics.
      RApps applications[ MAX_APPS ];      // application definitions
}
;

// Class variable that handles logging of messages
//CSMSCLog   ShmManager::smDefaultLogHandler("ShmManger");
//CSMSCLog* ShmManager::smShmLogHandler = &ShmManager::smDefaultLogHandler;

#define DB_MANAGER_NOT_AVAIL 1

const UINT16 sparedSegTag = 0x7727;

// The string for log messages.
TEXT dtstr[80];

//-------------------------------------------------------------
// METHOD
//    SmMgrLogFault
//
// DESCRIPTION
//  This function reports an fault to the log handler.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------

void ShmManager::SmMgrLogFault( int errCode, TEXT *method, int line, int sev = LOG_INFO, int sys_errno = 0, TEXT* msg = 0)
{
   TEXT lLogMsg[500];
   TEXT lTempMsg[10];

   sprintf(lTempMsg, "%d", errCode);
   strcpy(lLogMsg, "SHMEM ");
   strcat(lLogMsg, lTempMsg);
   sprintf(lTempMsg, " %d ", sev);
   strcat(lLogMsg, lTempMsg);
   strcat(lLogMsg, method);
   sprintf(lTempMsg, " %d ", line);
   strcat(lLogMsg, lTempMsg);
   if (msg != NULL)
      strcat(lLogMsg, msg);
   sprintf(lTempMsg, " %d ", sys_errno);
   strcat(lLogMsg, lTempMsg);

   //  cout << "sysLog :"  << lLogMsg << endl;

   //   syslog(LOG_INFO, "%s",lLogMsg);
   syslog(sev, "%s", lLogMsg);
   //    ShmManager::smShmLogHandler->GenerateLog( errCode, sev, msg );
}

//-------------------------------------------------------------
// METHOD
//    ShmManager
//
// DESCRIPTION        constructor
// Initialize member variables.  No shared memory segment is obtained.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------

ShmManager::ShmManager()
   //  : mNoOfSms(0),
   //    mConfigRead(0),
   //    mShmHdr(0),
   //    mOutOfMemoryHandler(0),
   :
      mLockSem()
{
   mNoOfSms = 0;
   mConfigRead = 0;
   mShmHdr = new ShMemHdr;
   mOutOfMemoryHandler = NULL;
   //   mLockSem();

   T (gShmEmTrace,
         cout << "ShmManager::ShmManager()" << endl;
     )
      memset (mSmSize, 0, sizeof(mSmSize));
   memset (mShmId, 0, sizeof(mShmId));
   memset (mSmPtr, 0, sizeof(mSmPtr));

   T (gShmEmTrace,
         cout << "ShmManager::ShmManager() Constructor OK" << endl;
     )

}  // ShmManager

//-------------------------------------------------------------
// METHOD
//    openConfigFile
//
// DESCRIPTION
// Read the config file, only once.
// Return 0 when everything is ok, -1 otherwise.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::OpenConfigFile (const TEXT *mFileName)
{
   if ( mConfigRead )
      return SHMM_SUCCESS;

   mConfigRead = 1;

   T (gShmEmTrace,
         cout << "ShmManager::Open Config file" << endl;
     )

      // Parse the configuration file information
      TEXT fname[80];
   if (0 == mFileName )
      strcpy(fname, getenv ("SMSC_SHMM_CONFIG"));
   else
      strcpy(fname, mFileName);
   if ( 0 == fname )
      strcpy(fname, "../etc/smsc_shm_mgr.cfg");

   mConfigFile.open (fname, O_RDONLY);
   T (gShmEmTrace,
         cout << "ShmManager::Open Config file success " << mConfigFile << endl;
     )
      return SHMM_SUCCESS;
}  // openConfigFile

//-------------------------------------------------------------
// METHOD
//    open
//
// DESCRIPTION
// Reading the configuration and obtain the shared memory segments if that's
// not done yet.  Attach to the shared memory segments afterwards.
// The existance of the first shared memory segments is the indicator
// whether all other segments have been created.
// RETURN: 0 if OK. Otherwise, errno returned from the ipc operation.
//        -1 when the segment size is incorrect.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::Open (const TEXT *mApplicationName, const TEXT *mConfigFileName, int mCreateIfNotPresent)
{
   unsigned int mIndex;
   TEXT *smPtr;
   TEXT mConfigStr[80];

   // The assert statement prevents the shared memory mgr from being opened twice.
   assert ( mShmId[0] == 0 );

   OpenConfigFile (mConfigFileName);

   if ((config.GetConfigData(mConfigFile, "SEMAPHORE", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
   }
   key_t mSemKey = atoi(mConfigStr);

   // Initialize or open the semaphore.  If we cannot get the semaphore, we are in trouble. There is no point of proceeding.
   if ( mLockSem.Create(mSemKey, 1) < 0 )
   {
      SmMgrLogFault( SHMM_FAIL_TO_GET_SEMA, "ShmManager()", __LINE__,
            LOG_CRIT, errno);
      return errno;
   }

   // Before we decide do anything, lock others out.  This prevents a race
   // condition.
   //   SHM_AUTO_LOCK(this);
   mLockSem.Wait();

   if ((config.GetConfigData(mConfigFile, "SHM_KEY0", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
         mLockSem.Signal();
      return -1;
   }
   key_t mShmKey = atoi(mConfigStr);

   // Try to obtain the first shared memory segment.  If it exists, we only
   // need to attach to it.  Otherwise, we may need to Create it.
   int shmId = shmget( mShmKey, 0, 0666 );
   if ( shmId < 0 )
   {
      if ( ! mCreateIfNotPresent )
      {
         SmMgrLogFault( SHMM_FAIL_TO_GET_SHM, "ShmManager()", __LINE__,
               LOG_CRIT, errno,
               "ShmManager(): Failed to get shared memory!" );
         mLockSem.Signal();
         return errno;
      }
      else
      {
         // Create() creates the shared memory segments and attach to them.
         // The reference count in the the CRefSemaphore allows another lock
         // operation.
         return Create(mApplicationName, mConfigFileName);
      }
   }
   mShmId[0] = shmId;

   // Attach to the shared memory
   smPtr = (TEXT*)shmat( (int)mShmId[0], 0, 0 );
   if ( smPtr == (void*) - 1 )
   {
      SmMgrLogFault( SHMM_ATTACH_TO_SHM_ERR, "ShmManager()", __LINE__,
            LOG_CRIT, errno,
            "ShmManager(): Fail to attach to the shared memory!" );
      mLockSem.Signal();
      return errno;
   }

   // initialize the members of this object.

   mSmPtr[0] = smPtr;
   mShmHdr = (ShMemHdr*)smPtr;
   mNoOfSms = mShmHdr->mNumPools;

   if ( mShmHdr->mNumPools == 1 )
      mSmSize[0] = mShmHdr->lastPoolSize;
   else
      mSmSize[0] = mShmHdr->maxPoolSize;

   for ( mIndex = 1; mIndex < mNoOfSms; mIndex++ )
   {
      if ( ( shmId = shmget( mShmHdr->poolKey[mIndex], 0, 0666 ) ) < 0 )
      {
         SmMgrLogFault( SHMM_FAIL_TO_GET_SHM, "ShmManager()", __LINE__, LOG_CRIT, errno,
               "ShmManager(): Failed to get shared memory!" );
         mLockSem.Signal();
         return errno;
      }
      mShmId[mIndex] = shmId;

      smPtr = (TEXT*)shmat( (int)mShmId[mIndex], 0, 0 );
      if ( smPtr == (TEXT*) - 1 )
      {
         SmMgrLogFault( SHMM_ATTACH_TO_SHM_ERR, "ShmManager()", __LINE__, LOG_CRIT, errno,
               "ShmManager(): Fail to attach to the shared memory!" );
         mLockSem.Signal();
         return errno;
      }
      mSmPtr[mIndex] = smPtr;

      if ( mIndex == mShmHdr->mNumPools - 1 )
         mSmSize[mIndex] = mShmHdr->lastPoolSize;
      else
         mSmSize[mIndex] = mShmHdr->maxPoolSize;
   }

   mLockSem.Signal();
   return 0;
}  // Open

//-------------------------------------------------------------
// METHOD
//    isAccessible
//
// DESCRIPTION
//
// RETURN:
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::IsAccessible() const
{
   return (mShmHdr != 0);
}  // isAccessible

//-------------------------------------------------------------
// METHOD
//    ~ShmManager
//
// DESCRIPTION      destructor
//  This destructor detaches the process from the shared memory.  We cannot
//  delete the shared memory because there may still be other processes
//  still using it.
//
// RETURN:
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
ShmManager::~ShmManager()
{
   //SHM_AUTO_LOCK(this);
   mLockSem.Wait();
   T (gShmEmTrace,
         cout << "ShmManager::~ShmManager()" << endl;
     )
      Close();
   mLockSem.Signal();
}  // ~ShmManager

//-------------------------------------------------------------
// METHOD
//    Create
//
// DESCRIPTION
// This allocates the shared memory and the associated
// semapgore. It then proceed to initialize the shared memory.
//
// RETURN: 0 if OK. Otherwise, errno returned from the ipc operation.
//       or -1 if the segment size is wrong.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::Create(const TEXT *mApplicationName,
      const TEXT *mConfigFileName)
{
   unsigned int totalSmSize;
   unsigned int maxPoolSize;
   unsigned int shmKey[ MAX_NMB_SHMEM_POOLS ];
   unsigned int shMemPoolSize, lastShMemPoolSize;
   unsigned int mIndex;

   T (gShmEmTrace,
         cout << "ShmManager::Create()" << endl;
     )

      T (gShmEmTrace,
            cout << "ShmManager::Create()" << mApplicationName << endl;
        )

      OpenConfigFile (mConfigFileName);

   TEXT mConfigStr[80];

   T (gShmEmTrace,
         cout << "ShmManager::Create() config file opened" << endl;
     )

      key_t mSemKey;
   if ((config.GetConfigData(mConfigFile, "SEMAPHORE", mConfigStr)) != true)
   {
      TERR (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
           )
         mSemKey = 9098;
   }
   else
      mSemKey = atoi(mConfigStr);

   if ((config.GetConfigData(mConfigFile, "MAX_SHMEM_POOL_SIZE", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
         maxPoolSize = 5 * 1024 * 1024;
   }
   else
      maxPoolSize = atoi(mConfigStr);

   if ((config.GetConfigData(mConfigFile, "TOTAL_SHMEM_SIZE", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
         totalSmSize = 5;
   }
   else
      totalSmSize = atoi(mConfigStr);

   if ((config.GetConfigData(mConfigFile, "NMB_SHMEM_POOLS", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
         mNoOfSms = 5;

   }
   else
      mNoOfSms = atoi(mConfigStr);

   T (gShmEmTrace,
         cout << "ShmManager::Create config variables" << endl
         << "Sem key 0 " << mSemKey << endl
         << "max pool size" << maxPoolSize << endl
         << "total size " << totalSmSize << endl;
     )

      // read the keys, these start at index 1 (not 0) in the shmKey
      // array, which is later copied to the initial block of
      // shared memory allocated above.
      for (mIndex = 1; mIndex <= mNoOfSms; mIndex++ )
      {
         TEXT var[100];
         sprintf (var, "SHM_KEY%d", mIndex );

         if ((config.GetConfigData(mConfigFile, var, mConfigStr)) != true)
         {
            T (gShmEmTrace,
                  cout << "ShmManager::Create unable to read from config file" << endl;
              )
               shmKey[mIndex] = 0;
         }
         shmKey[mIndex] = atoi(mConfigStr);
         if ( 0 == shmKey[mIndex] )
         {
            SmMgrLogFault( SHMM_SHMEM_MISSING_KEY, "allocShMem()", __LINE__,
                  LOG_CRIT, 0, var);
            return SHMM_SHMEM_CONFIG_ERR;
         }

         T (gShmEmTrace,
               cout << "ShmManager::Create config variables" << endl
               << "Sem key  " << shmKey[mIndex] << endl;
           )

      }  // for

   lastShMemPoolSize = totalSmSize - maxPoolSize * ( mNoOfSms - 1 );

   if ( mNoOfSms == 1 )
      shMemPoolSize = lastShMemPoolSize;
   else
      shMemPoolSize = maxPoolSize;

   if ( shMemPoolSize <= sizeof (ShMemHdr) )
   {
      SmMgrLogFault( SHMM_SHMEM_CONFIG_ERR, "allocShMem()", __LINE__,
            LOG_CRIT, 0,
            "allocShMem(): Shared memory pool size too small (config)" );
      return SHMM_SHMEM_CONFIG_ERR;
   }

   // Create the semaphore.
   int status = mLockSem.Create(mSemKey, 1);

   if ( status < 0 )
   {
      SmMgrLogFault( SHMM_CREATE_SEMA_ERR,
            "allocShMem()", __LINE__,
            LOG_CRIT, errno, 0 );
      return errno;
   }

   // Lock the semaphore
   mLockSem.Wait();

   // Create the master shared memory segment

   key_t mShmKey;
   if ((config.GetConfigData(mConfigFile, "SHM_KEY0", mConfigStr)) != true)
   {
      T (gShmEmTrace,
            cout << "ShmManager::Create unable to read from config file" << endl;
        )
         mShmKey = 9099;
   }
   else
      mShmKey = atoi(mConfigStr);

   mShmId[0] = shmget(mShmKey, (size_t)shMemPoolSize, 0666 | IPC_CREAT | IPC_EXCL);

   switch (errno)
   {
      case EINVAL :  // is returned if SHMMIN > size or size > SHMMAX, or size is greater than     size of segment.
         {
            T (gShmEmTrace,
                  cout << "Create Shm memory segemnt size error" << endl;
              )
               break;
         }

      case EEXIST :  // is returned if IPC_CREAT | IPC_EXCL  was  specified  and  the  segment                   exists.
         {
            T (gShmEmTrace,
                  cout << "Create shm memory flag error " << endl;
              )
               break;
         }

      case EIDRM :  // is returned if the segment is marked as destroyed, or was removed.
         {
            T (gShmEmTrace,
                  cout << "segment is markes as destroyed " << endl;
              )
               break;
         }

      case ENOSPC :  //  is  returned  if  all  possible  shared  memory  id's  have been taken
         // (SHMMNI) or if allocating a segment of the requested size would  cause
         // the  system to exceed the system-wide limit on shared memory (SHMALL).
         {
            T (gShmEmTrace,
                  cout << "no id left " << endl;
              )
               break;
         }

      case ENOENT :  //   is returned if no segment exists for the given key, and IPC_CREAT  was
         // not specified.
         {
            T (gShmEmTrace,
                  cout << "no segment left for key" << endl;
              )
               break;
         }

      case EACCES :  //  is  returned if the user does not have permission to access the shared
         //        memory segment.
         {

            T (gShmEmTrace,
                  cout << "permissions error " << endl;
              )
               break;
         }

      case ENOMEM :  //    is returned if no memory could be allocated for segment overhead.
         {
            T (gShmEmTrace,
                  cout << "no memory could be allocated " << endl;
              )
               break;
         }

   };

   T (gShmEmTrace,
         cout << "ShmManager::Create Shm Id[0] = " << mShmId[0] << endl;
     )

      if ( mShmId[0] == -1)
      {
         SmMgrLogFault( SHMM_CREATE_SHM_ERR,
               "allocShMem()", __LINE__,
               LOG_CRIT, errno, 0);
         mLockSem.Signal();
         return errno;
      }

   // We are the first to get the shared memory, we are
   // responsible for initializing it...

   TEXT* smPtr = (TEXT *)shmat( mShmId[0], 0, 0 );
   if (smPtr == (TEXT*) - 1 )
   {
      SmMgrLogFault( SHMM_ATTACH_TO_SHM_ERR,
            "allocShMem()", __LINE__,
            LOG_CRIT, errno, 0);
      mLockSem.Signal();
      return errno;
   }

   mSmPtr[0] = smPtr;
   mShmHdr = (ShMemHdr*)MemAlign(smPtr);

   // Initialize the whole shared memory system header to 0's
   memset ( mShmHdr, 0, sizeof (mShmHdr));

   mShmHdr->mLastMsgReport = time(0) - 1;
   mShmHdr->mFreePercent = 100;   // initially nothing is used.

   mShmHdr->poolSize[0] = (SIZE_T)shMemPoolSize;
   mShmHdr->poolId[0] = mShmId[0];
   mShmHdr->poolPtr[0] = smPtr;
   mShmHdr->poolKey[0] = (int)mShmKey;
   mShmHdr->mNumPools = mNoOfSms;
   mSmSize[0] = shMemPoolSize;

   // Set the begin address for the allocatable shared memory,
   // which is also now begin address for the spared segment.
   mShmHdr->mSegmentStart[0] = MemAlign(
         (TEXT*)mShmHdr + SizeRoundUP (sizeof(ShMemHdr)));

   // Calculate the end of the shared memory that is usable, i.e.
   // the last location that a rawNode can be located plus the size
   // of a rawNode
   TEXT* endp = smPtr + shMemPoolSize;
   rawNode* end_node = (rawNode*)MemAlign (endp - sizeof(rawNode));
   if ( (TEXT*)end_node + sizeof(rawNode) > (TEXT*)endp )
      end_node = end_node - 1;
   mShmHdr->mSegmentEnd[0] = (TEXT*)end_node;

   // Allocate a non-perishable node at the start and at the
   // end of the shared area.  This prevents joining of
   // freed blocks across shared memory segment boundaries.
   // The start node of the first shared memory segment also
   // functions as the sentinel element of the freelist
   //    allocation at the start
   rawNode* start_node = (rawNode*) mShmHdr->mSegmentStart[0];
   memset (start_node, 0, sizeof(rawNode));
   start_node->SetPrevSize(0);
   start_node->SetSize (sizeof(rawNode));
   start_node->SetInUse();
   // make the free list circular.
   start_node->next = start_node;
   start_node->prev = start_node;
   mShmHdr->mFreeList = start_node;

   //    allocation at the end pointed to be endraw
   memset (end_node, 0, sizeof(rawNode));
   end_node->SetPrevSize(0);
   end_node->SetSize (sizeof(rawNode));
   end_node->SetInUse();

   // initialize the area between the two nodes as the free list.
   // a free area.
   // The start_node is used as the sentinel element of the freelist,
   // which must never be removed.
   rawNode* first_node = (rawNode*)MemAlign ((TEXT*)(start_node + 1));

   memset(first_node, 0, sizeof(rawNode));
   first_node->SetPrevSize(start_node->Size());
   if ( end_node < first_node )
   {
      SmMgrLogFault( SHMM_ILLEGAL_MEMORY_SEG_SIZE,
            "allocShMem()", __LINE__,
            LOG_CRIT, -1, 0);
      mLockSem.Signal();
      return -1;
   }

   first_node->SetSize ((TEXT*)end_node - (TEXT*)first_node);
   first_node->Link (mShmHdr->mFreeList);

   end_node->SetPrevSize(first_node->Size());

   // update statistics
   mShmHdr->mNumFree = 1;
   mShmHdr->mFreeSpace = first_node->Size();

   mShmHdr->numApps = 0;

   mShmHdr->totalUsableSize = mShmHdr->mFreeSpace;
   mShmHdr->totalShMemSize = totalSmSize;
   mShmHdr->maxPoolSize = maxPoolSize;
   mShmHdr->lastPoolSize = lastShMemPoolSize;

   for ( mIndex = 1; mIndex < mNoOfSms; mIndex++ )
   {
      if ( mIndex == mNoOfSms - 1 )
         shMemPoolSize = lastShMemPoolSize;
      else
         shMemPoolSize = maxPoolSize;

      mShmId[mIndex] = shmget(shmKey[mIndex], (size_t)shMemPoolSize,
            0666 | IPC_CREAT | IPC_EXCL);
      if ( mShmId[mIndex] == -1 )
      {
         SmMgrLogFault (SHMM_CREATE_SHM_ERR, "allocShMem()", __LINE__,
               LOG_CRIT, errno, 0);
         mLockSem.Signal();
         return errno;
      }

      smPtr = (TEXT*)shmat (mShmId[mIndex], 0, 0 );
      if ( smPtr == (TEXT*) - 1)
      {
         SmMgrLogFault( SHMM_ATTACH_TO_SHM_ERR,
               "allocShMem()", __LINE__,
               LOG_CRIT, errno, 0);
         mLockSem.Signal();
         return errno;
      }
      mSmPtr[mIndex] = smPtr;

      mShmHdr->poolPtr[mIndex] = smPtr;
      mShmHdr->poolSize[mIndex] = (SIZE_T)shMemPoolSize;
      mShmHdr->poolId[mIndex] = mShmId[mIndex];
      mShmHdr->poolKey[mIndex] = shmKey[mIndex];

      // A segment will contain a node at the start and at the end,
      // which are permanently allocated. This prevents joining of
      // freed blocks across shared memory segment boundaries.
      //    allocation at the start
      start_node = (rawNode*)smPtr;
      memset (start_node, 0, sizeof(start_node));
      start_node->SetPrevSize(0);
      start_node->SetSize (sizeof(rawNode));
      start_node->SetInUse();

      //    allocation at the end
      endp = smPtr + shMemPoolSize;
      end_node = (rawNode*)MemAlign (endp - sizeof(rawNode));
      if ( (TEXT*)end_node + sizeof(rawNode) > endp )
         end_node = end_node - 1;
      memset (end_node, 0, sizeof(rawNode));
      end_node->SetSize(sizeof(rawNode));
      end_node->SetInUse();

      // all space between start and end is added to the free list
      first_node = (rawNode*)MemAlign ((TEXT*)(start_node + 1));
      if ( end_node < first_node )
      {
         SmMgrLogFault( SHMM_ILLEGAL_MEMORY_SEG_SIZE,
               "allocShMem()", __LINE__,
               LOG_CRIT, -1, 0);
         mLockSem.Signal();
         return -1;
      }

      first_node->SetSize ((TEXT*)end_node - (TEXT*)first_node);
      first_node->SetPrevSize(start_node->Size());
      first_node->Link(mShmHdr->mFreeList);

      end_node->SetPrevSize(first_node->Size());

      // update statistics
      mShmHdr->mNumFree++;
      mShmHdr->mFreeSpace += first_node->Size();
      mShmHdr->totalUsableSize = mShmHdr->mFreeSpace;

      // Set the end address for the allocatable shared memory.
      mShmHdr->mSegmentStart[mIndex] = (TEXT*)start_node;
      mShmHdr->mSegmentEnd[mIndex] = (TEXT*)end_node + sizeof(rawNode);
   }  // for

   mLockSem.Signal();
   return 0;
}  // Create

//-------------------------------------------------------------
// METHOD
//    close
//
// DESCRIPTION
// closes application access to the shared memory, but
// does not remove the shared memory, just detaches from it.
//
// RETURN:
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::Close ()
{
   int status = 0;

   for (unsigned int i = 0; i < mNoOfSms; i++ )
   {
      if ( mSmPtr[i] != 0 )
      {
         if ( shmdt( mSmPtr[i] ) < 0 )
         {
            SmMgrLogFault( SHMM_DEATTACH_SHM_ERR,
                  "ShmManager::close()", __LINE__,
                  LOG_CRIT, errno, 0);
            status = -1;
         }
         // this memory is no longer accessible, mark it as such
         mSmPtr[i] = 0;
      }
   }  // for

   // pointer to the header is no longer accessible, mark it as such
   mShmHdr = 0;

   return status;
}  // close

//-------------------------------------------------------------
// METHOD
//    deallocShMem
//
// DESCRIPTION
//
// This static function deallocates the shared memory and the associated
// semapgore. This fuction can be only called by the process  which
// allocated the shared memory, presumably the db manager.
//
//
// PARAMETER
//
// RETURN
//          0 if OK with ipc operation, otherwise errno.
//
// THROWS
//-------------------------------------------------------------
int ShmManager::DeallocShMem()
{
   shmid_ds shMemStat;
   unsigned int i;
   int status;

   T (gShmEmTrace,
         cout << "ShmManager::deallocShMem()" << endl;
     )

      // Lock the semaphore.
      //    SHM_AUTO_LOCK(this);
      mLockSem.Wait();

   // detach the shared memory
   status = Close();

   T (gShmEmTrace,
         cout << "Close status " << status << endl;
     )

      if ( status < 0 )
      {
         mLockSem.Signal();
         return errno;
      }

   for ( i = 0; i < mNoOfSms; i++ )
   {
      // Try to get the shared memory status.
      if ( shmctl( mShmId[i], IPC_STAT, &shMemStat ) < 0 )
      {
         SmMgrLogFault( SHMM_SHMCTL_GET_STATUS_ERR,
               "deallocShMem()", __LINE__,
               LOG_CRIT, errno, 0);
         mLockSem.Signal();

         T (gShmEmTrace,
               cout << "shmctl error " << status << endl;
           )

            return errno;
      }

      // Shared memory can only be removed by the process who created it.
      if ( shMemStat.shm_cpid != getpid() )
      {
         SmMgrLogFault( SHMM_ILLEGAL_USER,
               "deallocShMem()", __LINE__,
               LOG_WARNING, 0, 0);
         mLockSem.Signal();
         T (gShmEmTrace,
               cout << "creator pid is different from pid of this process " << status << endl;
           )

            return -1;
      }

      // How many processes are still attached to the shared memory.
      if ( shMemStat.shm_nattch > 0 )
      {
         sprintf( dtstr, "%d attached!", (unsigned int)shMemStat.shm_nattch );
         SmMgrLogFault( SHMM_STILL_IN_USE,
               "deallocShMem()", __LINE__,
               LOG_WARNING, 0, dtstr);
         T (gShmEmTrace,
               cout << "still " << dtstr << "processes are in use" << endl;
           )

      }  // if

      // Try to remove the shared memory.
      if ( shmctl( mShmId[i], IPC_RMID, 0 ) < 0 )
      {
         SmMgrLogFault( SHMM_FAIL_TO_REMOVE_SHM,
               "deallocShMem()", __LINE__,
               LOG_ERR, errno, 0);
         mLockSem.Signal();
         T (gShmEmTrace,
               cout << "shmctl rmid error " << status << endl;
           )
            return -1;
      }  // if
   }  // for

   status = mLockSem.Rm();
   if ( status < 0 )
   {
      SmMgrLogFault( SHMM_FAIL_TO_REMOVE_SEMA,
            "deallocShMem()", __LINE__,
            LOG_ERR, errno, 0);
      T (gShmEmTrace,
            cout << "semctl rmid error " << status << endl;
        )

   }
   else
   {
      // The deallocation of shared memory and its associated
      // semaphore is successfully done.
      SmMgrLogFault( SHMM_DEALLOCATED,
            "deallocShMem()", __LINE__,
            LOG_ERR, 0, 0);
   }

   mNoOfSms = 0;
   mSmSize[0] = 0;
   mShmId[0] = 0;

   mLockSem.Signal();
   T (gShmEmTrace,
         cout << "deallocation success " << status << endl;
     )

      return 0;
}  // deallocShMem

//-------------------------------------------------------------
// METHOD
//    shmNew
//
// DESCRIPTION
// Allocate a portion of the shared memory of a size equal or larger than the
// specified.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
void* ShmManager::ShmNew( const size_t bSize )
{
   //    SHM_AUTO_LOCK(this);
   mLockSem.Signal();

   rawNode *curPtr = 0;

   SIZE_T numbytes = SizeRoundUP(bSize + 2 * sizeof(SIZE_T) );

   // Check if the shared memory is over 90% full. Everytime that
   // there is a change in percentage a message will be generated.
   int free_percent = (int) (100.0 * mShmHdr->mFreeSpace /
         mShmHdr->totalUsableSize);
   if ( ( mShmHdr->mFreePercent <= 10 || free_percent <= 10 ) &&
         free_percent != mShmHdr->mFreePercent
      )
   {
      T (gShmEmTrace,
            cout << "free=" << free_percent
            << "   old free= " << mShmHdr->mFreePercent << endl;
        )
         sprintf( dtstr,
               "Over %d%% utilization. Total of %d bytes free of %u",
               (100 - free_percent), mShmHdr->mFreeSpace,
               mShmHdr->totalUsableSize );
      SmMgrLogFault( SHMM_SHM_ALMOST_FULL, "shMemNew()", __LINE__,
            LOG_ERR, 0, dtstr);
   }
   mShmHdr->mFreePercent = free_percent;

   // Search the linked list of free nodes.

   rawNode* firstFree = mShmHdr->mFreeList;

   // Strategies: Free list as FIFO vs. as LIFO.
   // FIFO: Divide the largest free block before reusing the freed blocks.
   // This gives the freed block a better chance to be merged.
   // Statistically, this strategy has the lest fragmentation in general
   // uses.  It's also faster.
   // LIFO: Use the most recently freed block has a better chance of
   // preserving the largest block so as to satisfy requirements that
   // irregular size requests.

   // Free list as LIFO.
   // for (curPtr = firstFree->next; curPtr != firstFree; curPtr = curPtr->next)

   // Free list as queue or FIFO.
   for (curPtr = firstFree->prev; curPtr != firstFree; curPtr = curPtr->prev)
   {
      if ( curPtr->Size() >= numbytes )
      {
         SIZE_T remainder = curPtr->Size() - numbytes;

         // This piece is large enough to hold the memory
         if ( remainder < MIN_TRUNCATE_SIZE )
         {
            // There is not enough room for another allocation
            curPtr->UnLink ();
            curPtr->SetInUse();

            // update the statistics
            mShmHdr->mNumFree--;
            mShmHdr->mFreeSpace -= curPtr->Size();

            mLockSem.Signal();
            return (void*)rawnode2mem(curPtr);
         }
         else
         {
            // Split the piece of memory into 2 pieces.
            // Take the memory from the back of the area, that
            // way you don't need to relink the freelist.
            rawNode* node = (rawNode*)MemAlign((TEXT*)curPtr + remainder);

            // update the next node
            rawNode* nnode = curPtr->NextNode();
            nnode->SetPrevSize(numbytes);

            curPtr->SetSize(remainder);

            node->SetPrevSize(remainder);
            node->SetSize (numbytes);
            node->SetInUse();

            // update the statistics
            mShmHdr->mFreeSpace -= numbytes;

            mLockSem.Signal();

            return (void*)rawnode2mem(node);
         }
      }
   }

   // Haven't found a large enough node.
   time_t now = time(0);
   if ( mShmHdr->mLastMsgReport != now )
   {
      if ( firstFree == curPtr )
      {
         SmMgrLogFault( SHMM_NOT_ENOUGH_SHM, "shMemNew()", __LINE__,
               LOG_CRIT, 0,
               "shMemNew(): Not enough Shared memory available!" );
      }
      else
      {
         SmMgrLogFault( SHMM_NOT_ENOUGH_SHM, "shMemNew()", __LINE__,
               LOG_CRIT, 0,
               "shMemNew(): logic error!!! finding memory" );
      }
      mShmHdr->mLastMsgReport = now;
   }

   if (mOutOfMemoryHandler)
   {
      (*mOutOfMemoryHandler)(this);     // give the control to the handler.
   }

   mLockSem.Signal();
   return 0;
}  // shMemNew

//-------------------------------------------------------------
// METHOD
//    shmDelete
//
// DESCRIPTION
// bPtr points to the memeory segment to be freed up.
// This avoid possible problem of using freed shared memory blocks.
// Adjecent free RawNodes will be coalesced.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
void ShmManager::ShmDelete( void* bPtr )
{
   //    SHM_AUTO_LOCK(this);

   mLockSem.Wait();

   int add_to_freelist = 1;

   // convert the pointer to a raw node
   rawNode* rnode = mem2rawnode(bPtr);
   int nb = rnode->Size();

   rawNode* pnode = rnode->PreviousNode();
   rawNode* nnode = rnode->NextNode();

   // see if it can be combined with the previous rawNode
   if (! pnode->InUse() )
   {
      add_to_freelist = 0;
      // combine with previous node
      pnode->SetSize (pnode->Size() + rnode->Size());
      nnode->SetPrevSize (pnode->Size());
      rnode = pnode;
   }

   // see if the node can be combined with the next node
   if ( ! nnode->InUse() )
   {
      add_to_freelist = ( rnode != pnode);

      // remove nnode from the free_list
      nnode->UnLink();
      mShmHdr->mNumFree--;
      rawNode* tnode = nnode->NextNode();
      rnode->SetSize (rnode->Size() + nnode->Size());
      tnode->SetPrevSize(rnode->Size());
   }

   // add the node to the freelist
   if ( add_to_freelist )
   {
      rnode->Link(mShmHdr->mFreeList);
      rnode->ClearInUse();
   }

   // update the statistics
   if ( add_to_freelist )
      mShmHdr->mNumFree++;
   mShmHdr->mFreeSpace += nb;

   mLockSem.Signal();
}  // shMemDelete

//-------------------------------------------------------------
// METHOD
//    registerApplication
//
// DESCRIPTION
// This method registers an application in the shared memory system
// header. A pointer to the header structure for the application
// will be allocated and returned to the caller.
// 0 is returned if the function fails.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
void * ShmManager::RegisterApplication( const TEXT* application, const size_t mHeaderSize)
{
   //    SHM_AUTO_LOCK(this);

   mLockSem.Wait();

   T (gShmEmTrace,
         cout << "ShmManager::registerApplication()" << endl;
     )

      if (ShMemNotAllocated())
         return 0;

   int mAtInd = RegisteredAt (application);

   if ( mAtInd < 0 )
   {
      if ( mShmHdr->numApps < MAX_APPS )
      {

         mAtInd = mShmHdr->numApps++;

         mShmHdr->applications[mAtInd].ptr = ShMemNew (mHeaderSize);
         if ( 0 == mShmHdr->applications[mAtInd].ptr )
         {
            sprintf( dtstr, "application %s mHeaderSize=%d",
                  application , mHeaderSize);
            SmMgrLogFault( SHMM_APP_HEADER_ALLOCATE,
                  "registerApplication()", __LINE__,
                  LOG_CRIT, 0, dtstr);
            mLockSem.Signal();
            return 0;
         }
         else
         {
            memset (mShmHdr->applications[mAtInd].ptr,
                  0, mHeaderSize);
         }
         strncpy (mShmHdr->applications[mAtInd].application,
               application, MAX_APP_NAME_LEN);
         mShmHdr->applications[mAtInd].application[MAX_APP_NAME_LEN] = 0;
         T (gShmEmTrace,
               cout << "Applicaiton name " << application << endl;
           )
      }
      else
      {
         sprintf( dtstr, "maximum number of applications is %d", MAX_APPS);
         SmMgrLogFault( SHMM_TOO_MANY_APPLICATIONS,
               "registerApplication()", __LINE__,
               LOG_CRIT, 0, dtstr);
      }
   }  // if

   if ( mAtInd < 0 )
   {
      mLockSem.Signal();
      T (gShmEmTrace,
            cout << "MATInd = 0  " << endl;
        )
         return 0;
   }
   else
   {
      mLockSem.Signal();
      T (gShmEmTrace,
            cout << "MATInd != 0  " << mShmHdr->applications[mAtInd].application<< endl;
        )
         return mShmHdr->applications[mAtInd].ptr;
   }

}  // registerApplication

//-------------------------------------------------------------
// METHOD
//    registerApplication
//
// DESCRIPTION
// This method checks to see if a application
// is already registered for using the shared memory and returns
// the index into the application table.
// -1 is returned if the application does not exist
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
int ShmManager::RegisteredAt ( const TEXT* application )
{
   //    SHM_AUTO_LOCK(this);

   //    mLockSem.Wait();

   T (gShmEmTrace,
         cout << "ShmManager::registeredAt()" << endl;
     )

      if (ShMemNotAllocated())
         return -1;

   T (gShmEmTrace,
         cout << "Number of Applications = "<< mShmHdr->numApps << endl;
     )
      for ( int i = 0; i < mShmHdr->numApps; i++ )
         if ( strcmp (mShmHdr->applications[ i ].application, application) == 0 )
         {
            //            mLockSem.Signal();
            return i;
         }

   //    mLockSem.Signal();
   T (gShmEmTrace,
         cout << "Returning -1"<< endl;
     )
      return -1;
}  // registeredAt

//-------------------------------------------------------------
// METHOD
//    registered
//
// DESCRIPTION
// This method checks to see if a application
// is already registered for using the shared memory and returns
// the pointer to its header structure.
//
// PARAMETER
//
// RETURN
//
// THROWS
//-------------------------------------------------------------
void* ShmManager::Registered( const TEXT* application )
{
   //    SHM_AUTO_LOCK(this);
   mLockSem.Wait();

   if (ShMemNotAllocated())
      return NULL;

   int mAtInd = RegisteredAt (application);

   T (gShmEmTrace,
         cout << "Registered at returned  "<< mAtInd << endl;
     )
      if ( mAtInd < 0 )
      {
         mLockSem.Signal();
         return NULL;
      }
      else
      {
         mLockSem.Signal();
         return mShmHdr->applications[ mAtInd ].ptr;
      }
}  // registered

void ShmManager::DeregisterApplication ( const TEXT* application )
   // This method deregisters the application. The application is to make
   // sure that all memory allocated was freed.
{
   //    SHM_AUTO_LOCK(this);
   mLockSem.Wait();

   T (gShmEmTrace,
         cout << "ShmManager::deregisterApplication()" << endl;
     )

      int mAtInd = RegisteredAt (application);

   if ( mAtInd < 0 )
   {
      mLockSem.Signal();
      return ;           // Application does not exist
   }

   // Copy the application entries following this application toward
   // the beginning of the table

   assert ( mAtInd < mShmHdr->numApps );

   memcpy (&mShmHdr->applications[mAtInd],
         &mShmHdr->applications[mAtInd + 1],
         (mShmHdr->numApps - mAtInd - 1) * sizeof (RApps) );

   // re-initialize the application area at the end.
   memset (&mShmHdr->applications[mShmHdr->numApps], 0, sizeof (RApps));

   // Decrement the number of applications.
   --mShmHdr->numApps;

   mLockSem.Signal();
}  // deregisterApplication

//-------------------------------------------------------------
// METHOD
//     shMemNotAllocated
//
// DESCRIPTION
//*  This method checkc if the shared memory is allocated by checking
//*  if mShmHdr is NULL or mShmId is 0.
//*
// PARAMETER
//
// RETURN
//            0 if allocated, otherwise 1.
//
// THROWS
//-------------------------------------------------------------
int ShmManager::ShMemNotAllocated() const
{
   if ((mShmHdr == NULL) || ( mShmId == 0 ))
   {
      SmMgrLogFault( SHMM_SHM_NOT_ALLOCATED, "shMemNotAllocated()", __LINE__);
      return 1;
   }
   return 0;
}

//-------------------------------------------------------------
// METHOD
//     set_loghandler
//
// DESCRIPTION
// class method to change the log handler//*
// PARAMETER
//
// RETURN
//            0 if allocated, otherwise 1.
//
// THROWS
//-------------------------------------------------------------
/*
   void ShmManager::SetLogHandler (CSMSCLog* newHandler)
   {
   smShmLogHandler = newHandler;
   }  // SetLogHandler

*/

//-------------------------------------------------------------
// METHOD
//     DumpApplicationHeader
//
// DESCRIPTION
// Print the header for the specified application if it exists, otherwise
// print a table of all the registered applications.

// PARAMETER
//
// RETURN
//
//
// THROWS
//-------------------------------------------------------------
void ShmManager::DumpApplicationHeader( const char* application )
{
   //   SHM_AUTO_LOCK(this);
   mLockSem.Wait();

   cout << "required application is " << application << endl;
   int at_ind = RegisteredAt (application);
   if ( at_ind < 0 )
   {
      cout << "application '" << application
         << "' does not exist in shared memory"
         << endl;
      cout << "registered applications:" << endl;
      for (int i = 0; i < mShmHdr->numApps; i++)
      {
         cout << "application " << i << " is "
            << mShmHdr->applications[i].application << endl;
      }  // for

      mLockSem.Signal();
      return ;
   }
   cout << "application in shared memory header at index: "
      << at_ind << endl;

   cout << "dumping not yet implemented" << endl;

   mLockSem.Signal();
}  // dumpApplicationHeader

//-------------------------------------------------------------
// METHOD
//     printSystemHeader
//
// DESCRIPTION
//
// PARAMETER
//
// RETURN
//
//
// THROWS
//-------------------------------------------------------------
ostream& ShmManager::PrintSystemHeader(ostream& ostr, int detailLevel)
{
   //   SHM_AUTO_LOCK(this);
   mLockSem.Wait();
   unsigned int i;

   ostr << "Shared memory manager system header: \n";
   ostr << "Total free space:           " << mShmHdr->mFreeSpace << endl;
   ostr << "Total shared memory:        " << mShmHdr->totalShMemSize << endl;
   ostr << "Total usable shared memory: " << mShmHdr->totalUsableSize << endl;
   ostr << "Free space percentage:      "
      << (100.0*mShmHdr->mFreeSpace / mShmHdr->totalUsableSize) << "%\n";

   ostr << "Max pool size:              " << mShmHdr->maxPoolSize << endl;
   ostr << "Last pool size:             " << mShmHdr->lastPoolSize << endl;
   ostr << "Number of pools:            " << mShmHdr->mNumPools << endl;

   char ibuf[10];
   char idbuf[10];
   ostr << "pool  id   key        ptr" << endl;
   ostr << "----  --   ---        ---" << endl;
   for ( i = 0; i < mShmHdr->mNumPools; i++)
   {
      sprintf (ibuf, "%2d", i);
      sprintf (idbuf, "%4d", mShmHdr->poolId[ i ]);
      ostr << ibuf
         << "    " << idbuf
         << " " << (void *)mShmHdr->poolKey [ i ]
         << " " << mShmHdr->poolPtr[ i ]
         << endl;
   }

   ostr << "app  headptr    name" << endl;
   ostr << "---  -------    ----" << endl;

   int mIndex;
   for (mIndex = 0; mIndex < mShmHdr->numApps; mIndex++)
   {
      sprintf (ibuf, "%2d", i);
      cout << ibuf
         << "   " << mShmHdr->applications[mIndex].ptr
         << " " << mShmHdr->applications[mIndex].application
         << endl;
   }

   ostr << "Nodes in the free list:     " << mShmHdr->mNumFree << endl;

   if (1 == detailLevel)
   {
      ostr << "Nodes in the free list are shown as (address, size):" << endl;

      rawNode* freelistPtr = mShmHdr->mFreeList->next;
      while (freelistPtr != mShmHdr->mFreeList)
      {
         ostr << " (" << freelistPtr << ", " << freelistPtr->Size() << ")";
         freelistPtr = freelistPtr->next;
      }
      ostr << endl;
   }

   mLockSem.Signal();
   return ostr;
}  // printSystemHeader

void ShmManager::Mydebug( )
{
   unsigned int i;

   cout << "nmbOfSms_: " << mNoOfSms << endl;
   cout << "totalSmSize: " << mShmHdr->totalShMemSize << endl;
   cout << "maxPoolSize: " << mShmHdr->maxPoolSize << endl;
   cout << "shmHdr_: " << (unsigned int)mShmHdr << endl;
   cout << "semaphore id: " << mLockSem.Id() << endl;
   for ( i = 0; i < mNoOfSms; i++ )
   {
      cout << "smPtr_[" << i << "]" << (unsigned int)mSmPtr[i] << endl;
      cout << "smSize_[" << i << "]" << mSmSize[i] << endl;
      cout << "shmId_[" << i << "]" << mShmId[i] << endl;
   }
   cout << "# free nodes:      " << mShmHdr->mNumFree << endl;
   cout << "total free space:  " << mShmHdr->mFreeSpace << endl;
   cout << "numPools: " << mShmHdr->mNumPools << endl;
   cout << "totalShMemSize: " << mShmHdr->totalShMemSize << endl;
   cout << "maxPoolSize: " << mShmHdr->maxPoolSize << endl;
   cout << "lastPoolSize: " << mShmHdr->lastPoolSize << endl;
   for ( i = 0; i < mNoOfSms; i++ )
   {
      cout << "segmentStart_m[" << i << "]" <<
         (unsigned int)mShmHdr->mSegmentStart[i] << endl;
      cout << "segmentEnd_m[" << i << "]" <<
         (unsigned int)mShmHdr->mSegmentEnd[i] << endl;
      cout << hex << "poolKey[" << i << "]" << mShmHdr->poolKey[i]
         << dec << endl;
      cout << "poolId[" << i << "]" << mShmHdr->poolId[i] << endl;
      cout << "poolPtr[" << i << "]" << (unsigned int)mShmHdr->poolPtr[i] << endl;
   }
}

