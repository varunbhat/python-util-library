//@(#) $Id: ShmManager.h,v 1.1 2002/08/06 06:40:28 root Exp $

//------------------------------------------------------------------------
// NAME : ShmManager.h
// fullpath/ShmManager -- Defines a shared memory manager
//      that can be used by different logical applications.
//
// COPYRIGHT
// BITL -- Copyright (C) 2001 BPL Innovision Technologies Ltd.,
// All rights reserved. No part of this computer program
// may be used or reproduced in any form by any
// means without prior written permission of
// BPL Innovision Technologies Ltd.
//
// CLASS TYPE:
//
// DESCRIPTION:
//    Policy and Strategy:
//       Pattern of Memory Reuse: Free list is organized as a queue (FIFO)
//          and allocation is sequential first fit.  This memory reuse policy
//          is the fastest in allocation/deallocation and gives freed nodes
//          a chance to grow. This policy is best for objects of a constant
//          size.  It performs poorly if many objects are allocated and many
//          different-sized free blocks accumulate.
//          [Wilson et.al] shows that FIFO sequential first fit is as good as
//          address ordered first fit and significantly better than LIFO
//          first fit and next fit.  Overtime, it tends toward behaving like a
//          best fit.
//       Splitting and Coalescing: Instant splitting and coalescing.
//       Fits: Split at any size.  The splitting threshold is two tags for
//          memory size (4 bytes each) and two addresses, i.e. 16 bytes
//          on 32bit-address-length OSs, and 24 bytes on
//          64bit-address-length OSs.
//
//    Mechanism:
//       Knuth & Standish's boundary tag is used for ultra-fast coalescing.
//       Each allocated memory block has two boundary tags, marking the size
//       of the previous block and the current block.  The last bit of the
//       current block size indicates whether the block is free.
//
//       Free node objects are linked on a circular double-linked-list.  This
//       has the advantage of not requiring tail checking.  There's a
//       'sentinel' element marking the beginning of the free list.  Newly
//       freed nodes are inserted at the head, i.e. right after the
//       'sentinel' node.
//
//    Performance:
//       This is an all-round fastest memory manager.
//       Comparing with earlier versions of the memory managers (on r1, r2 and
//       r3 branches),
//          allocation:       ~40% faster
//          deallocation:     ~220 times faster
//          initialization:   simpler & faster
//       Measurements are done with a buffer of 185bytes looping 5000 times.
//
//    Fragmentation:
//       The algorithm shows significant less fragmentation than previous
//       versions of shared memory managers.  For 8 MB of shared memory,
//       old versions of the program show signs of fragmentation when about
//       2MB of the space is used.  While the new version doesn't have much
//       fragmentation for the test program (benchHashDict.cc).
//
//    Overheads:
//       The boundary tags use no_of_node*8 bytes as the overhead.  A node
//       is created for every shmNew() request.  Each shmNew() request may
//       also have extra bytes to satisfy the word alignment. The word size
//       is defined by SHMEM_ALIGNMENT (which is 8).
//
//    Limitations:
//       Maximum 20 shared memory pools,
//       Maximum 50 applications,
//       Maximum 31 characters in application name.
//
//     Clean up dead code.  Document the usage and algorithm.  Measure the
//     performance and identify the weakness.  Add out-of-memory handler.
//
//     Add size fields before every allocated piece of shared, which
//     allows for faster insertion into the free list.
//     Change the btree organization of the free nodes into a double
//     linked list.
//     Pieces of allocated memory are thus maintained using a boundary tag
//     method as described by Knuth or Standish. For more information on
//     this see the paper by Paul Wilson et.al.
//     ftp://ftp.cs.utexas.edu/pub/garbage/allocsrv.ps
//
// Originated:   06-DEC-2001  Sadiq Mohammed
//
//-------------------------------------------------------------------------
#ifndef INCL_SHAREDMEMORY_H
#define INCL_SHAREDMEMORY_H

#include <sys/types.h>
#include <iostream.h>
#include "Semaphore.h"
#include "ConfigInfo.h"
#include "ctrace.h"
#include "syslog.h"


typedef enum shmError
{
    SHMM_SUCCESS = 0,
    SHMM_FAIL_TO_GET_SEMA,
    SHMM_FAIL_TO_GET_SHM,
    SHMM_ATTACH_TO_SHM_ERR,
    SHMM_CREATE_SHM_ERR,
    SHMM_CREATE_SEMA_ERR,
    SHMM_ILLEGAL_MEMORY_SEG_SIZE,
    SHMM_DEATTACH_SHM_ERR,
    SHMM_SHMCTL_GET_STATUS_ERR,
    SHMM_ILLEGAL_USER,
    SHMM_STILL_IN_USE,
    SHMM_FAIL_TO_REMOVE_SHM,
    SHMM_FAIL_TO_REMOVE_SEMA,
    SHMM_DEALLOCATED,
    SHMM_SHM_ALMOST_FULL,
    SHMM_NOT_ENOUGH_SHM,
    SHMM_APP_HEADER_ALLOCATE,
    SHMM_SHM_NOT_ALLOCATED,
    SHMM_SHMEM_MISSING_KEY,
    SHMM_TOO_MANY_APPLICATIONS,
    SHMM_SHMEM_CONFIG_ERR
};

const UINT16 MAX_NMB_SHMEM_POOLS = 20;
const int MAX_APP_NAME_LEN = 31;


// Structure for a free segment. They are organized in an unordered circular
// double link list.
struct rawNode;

// Shared memory header structure.
class ShMemHdr;

// Forward declaration of ShmManager.
class ShmManager;

// Type of a pointer to the function that handles the out of memory exception.
typedef void (*OutOfMemoryHandler)(ShmManager* memMgr);

// Shared memory manager class.
class ShmManager
{
    public:
        ShmManager();
        ~ShmManager();

        int Open (const TEXT *mApplicationName = "SHMM", const TEXT *mConfigFileName = 0, int mCreateIfNotPresent = 1);
        // Reading the configuration and obtain the shared memory segments
        // and semaphore if that's not done yet.  It attaches the process to
        // the shared memory segments.  The Open() function can only be called
        // once in one object.  Multiple invocations results in assert()
        // failure.

        int Create (const TEXT *mApplicationName = "SHMM", const TEXT* mConfigFileName = 0);
        // This method almost should be a private one.  It's recommend that
        // Open() is used instead.
        // This allocates the shared memory and the associated
        // semapgore. It then proceed to initialize the shared memory.
        // The create() function can only be called once in one object.
        // Multiple invocations results in assert() failure.

        int Close ();
        // closes application access to the shared memory, but
        // does not remove the shared memory, just detaches from it.

        int IsAccessible() const;
        // returns TRUE if the shared memory is accessible.

        int DeallocShMem();
        // Deallocate the managed shared memory segments. Shared memory can
        // only be removed by the process which created it, when there's no
        // other processes attaching to it.

        void* RegisterApplication( const TEXT* application, const size_t mHeaderSize );
        // This method registers an application in the shared memory system
        // header. A pointer to the header structure for the application
        // will be allocated and returned to the caller.
        // 0 is returned if the function fails.

        void DeregisterApplication ( const TEXT* application );
        // This method deregisters the application. The application is to make
        // sure that all memory allocated was freed.

        void* Registered( const TEXT* application );
        // This method checks to see if a application is already registered for
        // using the shared memory and returns the pointer to its header
        // structure.

        void* ShmNew( const size_t bSize );
        // Allocate a block shared memory of bSize.  Return the address of the
        // memory block or NULL if no more memory is left in the shared memory
        // pool.
        inline void* ShMemNew( const size_t bSize )
        {
            return ShmNew(bSize);
        };
        // For backward compatibility.  Not recommended for new code.

        void ShmDelete( void* bPtr );
        // Return the memory to the shared memory pool.  Warning: please
        // remember setting the point to the memory to NULL after delete().
        // Reusing the pointer will corrupt the shared memory.
        inline void ShMemDelete( void* bPtr, size_t )
        {
            ShmDelete(bPtr);
        };
        // For backward compatibility.  Not recommended for new code.

        //   inline int   Lock(ESemaMode mode = semaWriteLock)    { return mLockSem.Wait(mode); }
        //   inline int   Unlock() { return mLockSem.Signal(); }
        // These methods need to be called around accessing the shared
        // memory for read or write. The lock/unlock increments a
        // reference semaphore, which only locks/unlocks on the respectively
        // the first and last call.

        void DumpApplicationHeader( const TEXT* application );
        // Print the header for the specified application if it exists,
        // otherwise print a table of all the registered applications.

        ostream& PrintSystemHeader(ostream& ostr, int detailLevel = 0);
        // dump the shared memmory statistics.
        // detailLevel 0: show standard shared memory header.
        //             1: show the standard header + free list nodes.

        void Mydebug();
        // for debugging purpose.

        static size_t SizeAlign(size_t req);
        // roundup the size to the SHMEM_ALIGN_MASK (= 8) size.

        inline void SetOutOfMemoryHandler(OutOfMemoryHandler handler)
        {
            mOutOfMemoryHandler = handler;
        };
        // set the out-of-memory-handler to a user defined function.

        //static void SetLogHandler(CSMSCLog* newHandler);
        // dynamically change the log handler.

        static void SmMgrLogFault( int errCode, TEXT *loc, int line,
                                   int sev, int sys_errno, TEXT* msg );
        // public log generator function for now

    private:

        int OpenConfigFile(const TEXT * mFileName);
        int RegisteredAt(const TEXT* application);
        int ShMemNotAllocated() const;

        unsigned int mNoOfSms;

        TEXT* mSmPtr[ MAX_NMB_SHMEM_POOLS ];
        unsigned int mSmSize[ MAX_NMB_SHMEM_POOLS ];
        int mShmId[ MAX_NMB_SHMEM_POOLS ];

        int mConfigRead;
        ShMemHdr* mShmHdr;

        OutOfMemoryHandler mOutOfMemoryHandler;

        Semaphore mLockSem;    // semaphore used for lock/unlock protection
        // of the shared memory


        // static CSMSCLog  smDefaultLogHandler; //default_log_handler_sm;
        // static CSMSCLog* smShmLogHandler; //shm_log_handler_sm;

        ConfigInfo config;
        ifstream mConfigFile;
};


typedef struct registeredApplication
{
    TEXT application[MAX_APP_NAME_LEN];
    void*	ptr;
}
RApps;

#endif



