# include "MsgQueue.h"
#include <iostream>
using namespace std;

//---------------------------------------------------------------------
// METHOD      : MsgQueue
//---------------------------------------------------------------------
MsgQueue::MsgQueue()
{
    mMsgQueKey = 0;
    mMsgQueId = -1;
}

//---------------------------------------------------------------------
// METHOD      : ~MsgQueue
//---------------------------------------------------------------------
MsgQueue::~MsgQueue()
{
}

//---------------------------------------------------------------------
// METHOD      : ~GetMsggetError
//---------------------------------------------------------------------
msgQueueError MsgQueue::GetMsggetError(int errNumber)
{
    switch (errNumber)
    {
        case EACCES:
            // A message queue exists for key, but the calling
            // process has no access permissions to the queue.
            return Q_CREATE_PERMISSION_ERR;

        case EEXIST:
            // A message queue exists for key and msgflg was
            // asserting both IPC_CREAT and IPC_EXCL.
            return Q_FLAG_ERROR;

        case ENOENT:
            // No message queue exists for key and
            // msgflg wasn't asserting IPC_CREAT.
            return Q_NO_CREAT_FLAG;

        case ENOMEM:
            // A message queue has to be created but the system has
            // not enough memory for the new data structure.
            return Q_NOT_ENOUGH_MEM;

        case ENOSPC:
            // A message queue has to be created but the system limit
            // for the maximum number of message queues (MSGMNI)
            // would be exceeded.
            return Q_LIMIT_EXCEED;

        default:
            // unknown error
            return Q_UNKNOWN_ERR;
    }				//switch
}

//---------------------------------------------------------------------
// METHOD      : GetMsggetError
//---------------------------------------------------------------------
msgQueueError MsgQueue::GetMsgctlError(int errNumber)
{
    switch (errNumber)
    {
        case EACCES:
            // The argument cmd is equal to IPC_STAT but
            // the calling process has no read access permissions
            // on  the  message queue msqid.
            return Q_STATUS_PERMISSION_ERR;

        case EFAULT:
            // The argument cmd has value IPC_SET or IPC_STAT
            // but the address pointed to by buf isn't accessible.
            return Q_STATUS_INVALID_ADDRESS_ERR;

        case EIDRM:
            // The message queue was removed.
            return Q_NOT_EXIST;

        case EINVAL:
            // Invalid value for cmd or msqid.
            return Q_INVALID_COMMAND;

        case EPERM:
            // The  argument  cmd  has  value IPC_SET or IPC_RMID
            // but the calling process effective user-ID has insufficient
            // privileges to execute the command.  Note this is also the
            // case of a non super-user process trying to increase
            // the msg_qbytes value beyond the value specified by the
            // system parameter MSGMNB.
            return Q_DELETE_PERMISSION_ERR;

        default:
            // unknown error
            return Q_UNKNOWN_ERR;
    }				// switch
}

//---------------------------------------------------------------------
// METHOD      : GetMsgrcvError
// DESCRIPTION : 
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::GetMsgrcvError(int errNumber)
{
    switch (errNumber)
    {
        case E2BIG:
            // The message text length is greater than msgsz
            // and MSG_NOERROR isn't asserted in msgflg.
            return Q_MSG_LEN_ERR;

        case EACCES:
            // The calling process has no read access permissions
            // on the message queue.
            return Q_READ_PERM_ERR;

        case EFAULT:
            // The address pointed to by msgp isn't accessible.
            return Q_READ_BUF_ADDR_ERR;

        case EIDRM:
            // While the process was sleeping to receive a message,
            // the message queue was removed.
            return Q_REMOVED_ERR;

        case EINTR:
            // While the process was sleeping to receive a message,
            // the process received a signal that had to be caught.
            //printf (" Signal Error\n");
            return Q_READ_SIGNAL_ERR;

        case EINVAL:
            // Illegal msgqid value, or msgsz less than 0.
            return Q_READ_INVALID_ID;

        case ENOMSG:
            // IPC_NOWAIT was asserted in msgflg and no message of the
            // requested type existed on the message queue.
            return Q_NO_MSG_WITH_TYPE;

        default:
            // unknown error
            return Q_READ_UNKNOWN_ERR;
    }         // switch
}

//---------------------------------------------------------------------
// METHOD      : GetMsgsndError
// DESCRIPTION : 
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::GetMsgsndError(int errNumber)
{
    switch (errNumber)
    {
        case EAGAIN:
            // The message can't be sent due to the msg_qbytes
            // limit for the queue and IPC_NOWAIT was asserted in mgsflg.
            return Q_BYTES_LIMIT_ERR;

        case EACCES:
            // The calling process has no write access permissions
            // on the message queue.
            return Q_WRITE_PERM_ERR;

        case EFAULT:
            // The address pointed to by msgp isn't accessible.
            return Q_WRITE_BUF_ADDR_ERR;

        case EIDRM:
            // The message queue was removed.
            return Q_NOT_EXIST;

        case EINTR:
            // Sleeping on a full message queue condition,
            // the process received a signal that had to be caught.
            return Q_WRITE_SIGNAL_ERR;

        case EINVAL:
            // Invalid msqid value, or nonpositive mtype value,
            // or invalid msgsz value (less than 0 or greater than
            // the system value MSGMAX).
            return Q_WRITE_INVALID_ID;

        case ENOMEM:
            // The system has not enough memory to make a copy
            // of the supplied msgbuf.
            return Q_WRITE_NO_MEM;

        default:
            // unknown error
            return Q_WRITE_UNKNOWN_ERR;
    }
}

msgQueueError MsgQueue::Create(key_t msgKey, int perm, int32_t msgType)
{
    if (msgType <= 0)
        return Q_MSG_TYPE_ERR;

    mMsgQueId = msgget(msgKey, perm);

    if (mMsgQueId < 0)
        return this->GetMsggetError(errno);

    if (0 == mMsgQueId)
    {
        this->Remove();

        mMsgQueId = msgget(msgKey, perm);

        if (mMsgQueId < 0)
            return this->GetMsggetError(errno);
    }

    mMsgQueKey = msgKey;
    mMsgType = msgType;
    mPerm = perm;

    return Q_SUCCESS;
}

//---------------------------------------------------------------------
// METHOD      : Remove
//---------------------------------------------------------------------
msgQueueError MsgQueue::Remove()
{
    if ((msgctl(mMsgQueId, IPC_RMID, 0)) != 0)
        return this->GetMsgctlError(errno);

    return Q_SUCCESS;
}

//---------------------------------------------------------------------
// METHOD      : GetQueueStatus
// DESCRIPTION : returns the status about the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::GetQueueStatus(struct msqid_ds * pStatus)
{
    if ((msgctl(mMsgQueId, IPC_STAT, pStatus)) != 0)
//   {
//      TERR (gQueTrace, cout << "MsgQueue::"
//            << " QueKey:" << mMsgQueKey << " QueId:" << mMsgQueId
//            << " IPC stat error " << errno << endl;);
//   }				// if
//
//   T (gQueTrace, cout << "MsgQueue::"
//         << " QueKey:" << mMsgQueKey << " QueId:" << mMsgQueId
//         << " msg_lspid:" << pStatus->msg_lspid
//         << " msg_lrpid:" << pStatus->msg_lrpid
//         << " msg_qnum:" << pStatus->msg_qnum
//         << " Get IPC Status success" << endl;);

        return Q_SUCCESS;
}

//---------------------------------------------------------------------
// METHOD      : GetQueueKey
// DESCRIPTION : returns the status about the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
key_t MsgQueue::GetQueueKey()
{
    return mMsgQueKey;
}

///////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------
// METHOD      : HandleQueReadError
// DESCRIPTION : Handle Queue Read Error
// PARAMETER   : msgQueueError
// RETURN      : bool
//---------------------------------------------------------------------
bool MsgQueue::HandleQueReadError(msgQueueError err, modeOption mode)
{
    switch (err)
    {
        case Q_NOT_EXIST:
            case Q_REMOVED_ERR:
            case Q_READ_INVALID_ID:
            if (Q_SUCCESS != this->Create(mMsgQueKey, mPerm))
                return false;

            return true;

        case Q_NO_MSG_WITH_TYPE:
            if (NON_BLOCKING == mode)
                usleep(25000);

            return true;

        default:
            return false;
    }
}

//---------------------------------------------------------------------
// METHOD      : ReadMsgWithType
// DESCRIPTION : 
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::ReadMsgWithType(void *pData, ssize_t dataLen,
        int32_t & rMsgType, modeOption mode)
{
    // if size of given data buffer exceeds max limit return error
    if (dataLen > MAX_MSGQUE_DATA_SIZE || dataLen <= 0)
        return Q_MSG_LEN_ERR;

    __MsgQueData lMsg;
    msgQueueError lMsgQueErr = Q_UNKNOWN_ERR;
    ssize_t lDataLen = 0;

    int32_t lMsgType = rMsgType;

    if (BLOCKING == mode)
        lDataLen = msgrcv(mMsgQueId, &lMsg, dataLen, lMsgType, 0);
    else
        lDataLen = msgrcv(mMsgQueId, &lMsg, dataLen, lMsgType, IPC_NOWAIT);

    int lErrNo = errno;

    if (lDataLen == dataLen)
    {
        // copy read parameters
        memcpy(pData, (void *) &lMsg.mText, dataLen);
        rMsgType = lMsg.mType;

        return Q_SUCCESS;
    }

    if (lDataLen < 0)
    {
        lMsgQueErr = this->GetMsgrcvError(lErrNo);

        this->HandleQueReadError(lMsgQueErr, mode);

        return lMsgQueErr;
    }
    return Q_NO_MSG_WITH_TYPE;
}

//---------------------------------------------------------------------
// METHOD      : ReadMsg
// DESCRIPTION : reads data from the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::ReadMsg(void *pData, ssize_t dataLen, int32_t msgType,
        modeOption mode)
{
    int32_t lMsgType = msgType;

    return this->ReadMsgWithType(pData, dataLen, lMsgType, mode);
}

//---------------------------------------------------------------------
// METHOD      : ReadMsg
// DESCRIPTION : reads data from the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::ReadMsg(void *pData, ssize_t dataLen, modeOption mode)
{
    int32_t lMsgType = mMsgType;

    return this->ReadMsgWithType(pData, dataLen, lMsgType, mode);
}

///////////////////////////////////////////////////////////////////////
//-------------------------------------------------------------------//
///////////////////////////////////////////////////////////////////////
//---------------------------------------------------------------------
// METHOD      : HandleQueWriteError
// DESCRIPTION : Handle Queue Write Error
// PARAMETER   : msgQueueError
// RETURN      : bool
//---------------------------------------------------------------------
bool MsgQueue::HandleQueWriteError(msgQueueError err, modeOption mode)
{
    switch (err)
    {
        case Q_WRITE_INVALID_ID:
            case Q_NOT_EXIST:
            if (Q_SUCCESS != this->Create(mMsgQueKey, mPerm))
                return false;

            return true;

        case Q_WRITE_SIGNAL_ERR:
            return true;

        case Q_BYTES_LIMIT_ERR:
            case Q_WRITE_NO_MEM:
            if (BLOCKING == mode)
                usleep(5000);

            return true;

        default:
            return false;
    }
}

//---------------------------------------------------------------------
// METHOD      : 
// DESCRIPTION : writes data into the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
        modeOption mode, int timeout)
{
    if (msgType <= 0)
        return Q_MSG_TYPE_ERR;

    // if size of given data buffer exceeds max limit return error
    if (dataLen > MAX_MSGQUE_DATA_SIZE || dataLen < 0)
        return Q_MSG_LEN_ERR;

    if (mMsgQueId <= 0)
        return Q_WRITE_INVALID_ID;

    __MsgQueData lMsg;
    msgQueueError lMsgQueErr = Q_UNKNOWN_ERR;
    int lRetVal;
    time_t lExpTime = time(NULL) + timeout;

    lMsg.mType = msgType;

    memcpy((void *) &lMsg.mText, pData, dataLen);

    do
    {
        lRetVal = msgsnd(mMsgQueId, &lMsg, dataLen, IPC_NOWAIT);

        int lErrNo = errno;

        if (0 == lRetVal)
            return Q_SUCCESS;
        lMsgQueErr = this->GetMsgsndError(lErrNo);

        if (true != this->HandleQueWriteError(lMsgQueErr, mode))
            return lMsgQueErr;
        if (BLOCKING != mode)
            return lMsgQueErr;
    }
    while (lExpTime > time(NULL));

    return Q_TIMEOUT;
}

//---------------------------------------------------------------------
// METHOD      : WriteMsg
// DESCRIPTION : writes data into the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
        modeOption mode)
{
    return this->WriteMsg(pData, dataLen, msgType, mode,
    DEFAULT_MSGQUE_BLOCK_TIME);
}

//---------------------------------------------------------------------
// METHOD      : WriteMsg
// DESCRIPTION : writes data into the queue
// PARAMETER   : 
// RETURN      : msgQueueError
//---------------------------------------------------------------------
msgQueueError MsgQueue::WriteMsg(void *pData, ssize_t dataLen, modeOption mode)
{
    return this->WriteMsg(pData, dataLen, mMsgType, mode,
    DEFAULT_MSGQUE_BLOCK_TIME);
}
