#ifndef INCL_MSGQUEUE_H
#define INCL_MSGQUEUE_H

//----------------------------------------------------------------------
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

//----------------------------------------------------------------------
#define DEFAULT_MSGQUE_PERM       IPC_CREAT|0666
#define DEFAULT_MSGQUE_BLOCK_TIME 60
#define DEFAULT_MSGQUE_MSGTYPE    1
#define MAX_MSGQUE_DATA_SIZE      4096

//----------------------------------------------------------------------
enum msgQueueError
{
    // General
    Q_SUCCESS = 0,
    Q_UNKNOWN_ERR,
    Q_MSG_TYPE_ERR,
    Q_NOT_EXIST,
    Q_TIMEOUT,

    // create errors
    Q_CREATE_PERMISSION_ERR = 100,
    Q_FLAG_ERROR,
    Q_REMOVAL_MARKED,
    Q_NO_CREAT_FLAG,
    Q_NOT_ENOUGH_MEM,
    Q_LIMIT_EXCEED,

    //remove errors
    Q_DELETE_PERMISSION_ERR = 200,
    Q_DELETE_INVALID_SECOND_ARG,
    Q_INVALID_COMMAND,

    // read errors
    Q_MSG_LEN_ERR = 300,
    Q_READ_PERM_ERR,
    Q_READ_BUF_ADDR_ERR,
    Q_REMOVED_ERR,
    Q_READ_SIGNAL_ERR,
    Q_READ_INVALID_ID,
    Q_READ_UNKNOWN_ERR,
    Q_NO_MSG_WITH_TYPE,

    // write errors
    Q_BYTES_LIMIT_ERR = 400,
    Q_WRITE_PERM_ERR,
    Q_WRITE_BUF_ADDR_ERR,
    Q_WRITE_SIGNAL_ERR,
    Q_WRITE_INVALID_ID,
    Q_WRITE_NO_MEM,
    Q_WRITE_UNKNOWN_ERR,

    //get status errors
    Q_STATUS_PERMISSION_ERR,
    Q_STATUS_INVALID_ADDRESS_ERR
};

enum modeOption
{
    BLOCKING = 0, NON_BLOCKING = 1
};

struct __MsgQueData
{
        int32_t mType;
        char mText[MAX_MSGQUE_DATA_SIZE];
};

//----------------------------------------------------------------------
//----------------------------------------------------------------------
class MsgQueue
{
    public:
        // constructor
        MsgQueue();

        //destructor
        ~MsgQueue();

        // create
        msgQueueError Create(key_t msgKey, int perm = DEFAULT_MSGQUE_PERM,
                int32_t msgType = DEFAULT_MSGQUE_MSGTYPE);

        // remove
        msgQueueError Remove();

        // Status
        msgQueueError GetQueueStatus(struct msqid_ds *pStatus);

        key_t GetQueueKey();

        // Read
        msgQueueError ReadMsgWithType(void *pData, ssize_t dataLen,
                int32_t & rMsgType, modeOption mode = BLOCKING);

        msgQueueError ReadMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode = BLOCKING);
        msgQueueError ReadMsg(void *pData, ssize_t dataLen, modeOption mode =
                BLOCKING);

        // Write
        msgQueueError WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode, int timeout);
        msgQueueError WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode = BLOCKING);
        msgQueueError WriteMsg(void *pData, ssize_t dataLen, modeOption mode =
                BLOCKING);

    private:
        msgQueueError GetMsgctlError(int errNumber);
        msgQueueError GetMsggetError(int errNumber);
        msgQueueError GetMsgrcvError(int errNumber);
        msgQueueError GetMsgsndError(int errNumber);

        bool HandleQueWriteError(msgQueueError, modeOption mode);
        bool HandleQueReadError(msgQueueError, modeOption mode);

        void SendHeartBeatResponse(int32_t msgType);

        int mPerm;
        int32_t mMsgType;

        key_t mMsgQueKey;
        int mMsgQueId;
};

#endif
