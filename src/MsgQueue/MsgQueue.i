/* File: example.i */
%module MsgQueue

%{
#define INCL_PY_MESSAGE_QUEUE
#include "MsgQueue.h"
%}

class MsgQueue
{
    public:
        MsgQueue();
        ~MsgQueue();

        msgQueueError Create(key_t msgKey, int perm = DEFAULT_MSGQUE_PERM,
                int32_t msgType = DEFAULT_MSGQUE_MSGTYPE);
        msgQueueError Remove();
        msgQueueError GetQueueStatus(struct msqid_ds *pStatus);

        key_t GetQueueKey();
        msgQueueError ReadMsgWithType(void *pData, ssize_t dataLen,
                int32_t & rMsgType, modeOption mode = BLOCKING);

        msgQueueError ReadMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode = BLOCKING);
        msgQueueError ReadMsg(void *pData, ssize_t dataLen, modeOption mode =
                BLOCKING);

        msgQueueError WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode, int timeout);
        msgQueueError WriteMsg(void *pData, ssize_t dataLen, int32_t msgType,
                modeOption mode = BLOCKING);
        msgQueueError WriteMsg(void *pData, ssize_t dataLen, modeOption mode =
                BLOCKING);

    private:
        msgQueueError GetMsgctlError(int errNumber);
        msgQueueError GetMsggetError(int errNumber);
        msgQueueError GetMsgrcvError(int errNumber);
        msgQueueError GetMsgsndError(int errNumber);

        bool HandleQueWriteError(msgQueueError, modeOption mode);
        bool HandleQueReadError(msgQueueError, modeOption mode);

        void SendHeartBeatResponse(int32_t msgType);

        int mPerm;
        int32_t mMsgType;

        key_t mMsgQueKey;
        int mMsgQueId;
};

