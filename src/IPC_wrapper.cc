#include <Python.h>
#include <iostream>
#include <structmember.h>
#include <bytes_methods.h>
#include "MsgQueue.h"

using namespace std;

struct Py_MsgQueue
{
        PyObject_HEAD
        PyObject * key;
        MsgQueue mMsgQueue;
};

static void Py_MsgQueue_dealloc(Py_MsgQueue* self)
{
    self->ob_type->tp_free((PyObject*) self);
}

static PyObject* Py_MsgQueue_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    Py_MsgQueue *self;
    self = (Py_MsgQueue *) type->tp_alloc(type, 0);
    self->key = PyInt_FromLong(0);
    if (self->key == NULL)
    {
        Py_DECREF(self);
        return NULL;
    }
    return (PyObject *) self;
}

static int Py_MsgQueue_init(Py_MsgQueue *self, PyObject *args, PyObject *kwds)
{
    int lQkey = 0, lPerm = 0, lMsgType = -1;

    static char *kwlist[] = { "qkey", "perm", "msgtype", NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i|ii",
            kwlist, &lQkey, &lPerm, &lMsgType))
        return -1;

    if (lQkey == 0)
        return -1;
    if (lPerm == 0)
        lPerm = DEFAULT_MSGQUE_PERM;
    if (lMsgType == -1)
        lMsgType = DEFAULT_MSGQUE_MSGTYPE;

    if (Q_SUCCESS != self->mMsgQueue.Create(lQkey, lPerm, lMsgType))
        return -1;

    return 0;
}

static PyObject* Py_MsgQueue_ReadMsg(Py_MsgQueue* self, PyObject* args, PyObject *kwds)
{
    int lMsglen = 0, lMsgtype = 0;
    modeOption lMode = BLOCKING;
    char * tempMsgqData = NULL;

    static char * kwlist[] = { "len", "msgtype", "mode", NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i|ii",
            kwlist, &lMsglen, &lMsgtype, &lMode))
        Py_RETURN_NONE;

    tempMsgqData = (char*) malloc(lMsglen * sizeof(char));

    msgQueueError lqErr = self->mMsgQueue.ReadMsg(&tempMsgqData,
            lMsglen, lMsgtype, lMode);
    PyObject * retval = PyByteArray_FromStringAndSize(tempMsgqData, lMsglen);
    Py_INCREF(retval);
    return retval;
}

static PyObject* Py_MsgQueue_WriteMsg(Py_MsgQueue* self, PyObject* args, PyObject *kwds)
{
    PyObject *lRawDataString;
    modeOption lMode = BLOCKING;
    int lMsgType = DEFAULT_MSGQUE_MSGTYPE;

    static char * kwlist[] = { "data", "msgtype", "mode", NULL };
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O|ii", kwlist, &lRawDataString, &lMsgType, &lMode))
        Py_RETURN_NONE;

    const char* s = PyByteArray_AS_STRING(lRawDataString);
    char* lrawcString = PyByteArray_AsString(lRawDataString);
    size_t lrawcStrLen = PyByteArray_Size(lRawDataString);
    self->mMsgQueue.WriteMsg(lrawcString, lrawcStrLen, lMsgType, lMode);

    Py_RETURN_TRUE;
}

static PyMemberDef Py_MsgQueue_members[] = {
        { NULL }
};

static PyMethodDef Py_MsgQueue_methods[] = {
        { "ReadMsg", (PyCFunction) Py_MsgQueue_ReadMsg, METH_VARARGS | METH_KEYWORDS, "Read from Msg Queue" },
        { "WriteMsg", (PyCFunction) Py_MsgQueue_WriteMsg, METH_VARARGS | METH_KEYWORDS, "Write to Msg Queue" },
        { NULL }
};

static PyTypeObject MsgQueueType =
{
    PyObject_HEAD_INIT(NULL)
    0, /*ob_size*/
    "IPC.MsgQueue", /*tp_name*/
    sizeof(Py_MsgQueue), /*tp_basicsize*/
    0, /*tp_itemsize*/
    (destructor)Py_MsgQueue_dealloc, /*tp_dealloc*/
    0, /*tp_print*/
    0, /*tp_getattr*/
    0, /*tp_setattr*/
    0, /*tp_compare*/
    0, /*tp_repr*/
    0, /*tp_as_number*/
    0, /*tp_as_sequence*/
    0, /*tp_as_mapping*/
    0, /*tp_hash */
    0, /*tp_call*/
    0, /*tp_str*/
    0, /*tp_getattro*/
    0, /*tp_setattro*/
    0, /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "API to read from the message queue and return "
    "the resulting value in binary format", /* tp_doc */
    0, /* tp_traverse */
    0, /* tp_clear */
    0, /* tp_richcompare */
    0, /* tp_weaklistoffset */
    0, /* tp_iter */
    0, /* tp_iternext */
    Py_MsgQueue_methods, /* tp_methods */
    Py_MsgQueue_members, /* tp_members */
    0, /* tp_getset */
    0, /* tp_base */
    0, /* tp_dict */
    0, /* tp_descr_get */
    0, /* tp_descr_set */
    0, /* tp_dictoffset */
    (initproc)Py_MsgQueue_init, /* tp_init */
    0, /* tp_alloc */
    Py_MsgQueue_new, /* tp_new */
};

static PyMethodDef module_methods[] = { { NULL } };

#ifndef PyMODINIT_FUNC  /* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif

PyMODINIT_FUNC initIPC(void)
{
    PyObject* m;

    if (PyType_Ready(&MsgQueueType) < 0)
        return;

    m = Py_InitModule3("IPC", module_methods,
            "Has all the module necessary for IPC");

    if (m == NULL)
        return;

    if (-1 == PyModule_AddIntMacro(m, IPC_CREAT))
        return;

//    Error = PyErr_NewException("", NULL, NULL);
//    Py_INCREF (SpamError);
    PyModule_AddObject(m, "MsgQueue", (PyObject *) &MsgQueueType);
    Py_INCREF (&MsgQueueType);
}

