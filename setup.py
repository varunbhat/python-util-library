from distutils.core import Extension
from distutils.core import setup

extensions = [Extension('IPC',
                         sources=['src/MsgQueue/MsgQueue.cc',
                                  "src/IPC_wrapper.cc"],
                         include_dirs=['src/MsgQueue', 'src/SHM'],
                        ),
             ]

setup(name='util',
      version='0.1',
      description=open('README', 'r').read(),
      author='Varun Bhat',
      author_email='varunbhat.kn@gmail.com',
      url='http://thegeektronics.com',
      package_dir={'util': 'src'},
      py_modules=['util.ConfigRead'],
      ext_package='util',
      ext_modules=extensions
     )
